import Vue from 'vue'
import VueRouter from 'vue-router';
import App from './App.vue';
import startGame from './components/start-game.vue';
import healthBlock from './components/health-block.vue';
import endGame from './components/end-game.vue';
// import 
Vue.use(VueRouter);
const routes = [
  { path: '/endGame', component: endGame },
  { path: '/fight', component: healthBlock },
  { path: '/', component: startGame },
]
const router = new VueRouter({
  routes,
  mode: 'history'
})
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
